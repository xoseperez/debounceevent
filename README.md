# DebounceEvent

Simple push button and toggle switch debounce library for Arduino and ESP8266 that reports number of clicks and length

This was the official repository for DebounceEvent library until August 30th, 2018, when it was moved over to GitHub. 
Please visit the new repository to checkout the latest version of the firmware, download up to date images, read the documentation, report issues or file pull-requests.

https://github.com/xoseperez/debounceevent